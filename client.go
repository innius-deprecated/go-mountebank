package mountebank

import (
"fmt"
"gopkg.in/resty.v1"
)

var (
	BaseURL string = "http://localhost:2525"
)

func SetDebug(debug bool) {
	resty.SetDebug(debug)
}

// IsRunning checks if mountebank is up and running;
func IsRunning() bool {
	var handler = func() (*resty.Response, error) {
		return resty.R().Get(BaseURL)
	}
	return invoke(handler, 200) == nil
}

func createImposter(i *Imposter) (*Imposter, error) {
	result := &Imposter{}
	var handler = func() (*resty.Response, error) {
		return resty.R().SetBody(i).SetResult(result).Post(BaseURL + "/imposters")
	}
	if err := invoke(handler, 201); err != nil {
		return nil, err
	}

	return result, nil
}

func deleteImposter(i *Imposter) error {
	var handler = func() (*resty.Response, error) {
		return resty.R().SetBody(i).Delete(fmt.Sprintf("%v/imposters/%v", BaseURL, i.Port))
	}
	return invoke(handler, 200)
}

func deleteImposters() error {
	var handler = func() (*resty.Response, error) {
		return resty.R().Delete(fmt.Sprintf("%v/imposters", BaseURL))
	}
	return invoke(handler, 200)
}

type ServiceStatusError struct {
	StatusCode int
	Message    string
}

func (se *ServiceStatusError) Error() string {
	return fmt.Sprintf("service returned an unexpected status: status: %v; message: %v.", se.StatusCode, se.Message)
}

type handler func() (*resty.Response, error)

func invoke(h handler, expected int) error {
	resp, err := h()
	if err != nil {
		return err
	}

	if resp.StatusCode() == expected {
		return nil
	}

	return &ServiceStatusError{
		StatusCode: resp.StatusCode(),
		Message:    resp.String(),
	}

}
