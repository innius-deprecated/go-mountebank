module bitbucket.org/to-increase/go-mountebank

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
	golang.org/x/net v0.0.0-20181114220301-adae6a3d119a // indirect
	gopkg.in/resty.v1 v1.10.2
)
