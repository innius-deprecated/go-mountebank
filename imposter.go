package mountebank

func NewImposter() *Imposter {
	return &Imposter{
		Protocol: "http",
	}
}

type Imposter struct {
	Port     int     `json:"port,omitempty"`
	Protocol string  `json:"protocol"`
	Stubs    []*Stub `json:"stubs"`
}

func (imp *Imposter) WithStub(s *Stub) *Imposter {
	imp.Stubs = append(imp.Stubs, s)
	return imp
}

func (imp *Imposter) WithPort(port int) *Imposter {
	imp.Port = port
	return imp
}

func (imp *Imposter) Create() (*Imposter, error) {
	r, err := createImposter(imp)
	if err != nil {
		return nil, err
	}
	imp.Port = r.Port
	return imp, nil
}

func (imp *Imposter) Delete() error {
	return deleteImposter(imp)
}

func DeleteAllImposters() error {
	return deleteImposters()
}
