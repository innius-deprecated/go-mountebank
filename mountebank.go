package mountebank

import (
	"net/http"
)

// DefaultPostImposter creates a default imposter with a stubs for a POST endpoint and not found
// path     : the path of the endpoint
// body     : the body returend by http post
// status   : the http status code of the stub
func DefaultPostImposter(path string, body interface{}, status int, port int) (*Imposter, error) {
	return NewImposter().WithStub(defaultStub(POST, path, body, status, 0)).WithPort(port).WithStub(notFoundStub()).Create()
}

// DefaultGetImposter creates a default imposter with a stubs for a GET endpoint and not found
// path     : the path of the endpoint
// body     : the body returend by http post
// status   : the http status code of the stub
func DefaultGetImposter(path string, body interface{}, status int, port int) (*Imposter, error) {
	return NewImposter().WithStub(defaultStub(GET, path, body, status, 0)).WithPort(port).WithStub(notFoundStub()).Create()
}

// DefaultImposter creates a default imposter with a stubs for a GET endpoint and not found
// method   : the http method of the stub
// path     : the path of the endpoint
// body     : the body returend by http post
// status   : the http status code of the stub
func DefaultImposter(method, path string, body interface{}, status int, wait int, port int) (*Imposter, error) {
	return NewImposter().WithStub(defaultStub(method, path, body, status, wait)).WithPort(port).WithStub(notFoundStub()).Create()
}

func defaultStub(method, path string, body interface{}, status int, wait int) *Stub {
	return NewStub().
		WithPredicate(NewPredicate().WithEquals().WithPath(path).WithMethod(method)).
		WithResponse(NewResponse().WithBody(body).WithStatusCode(status).WithWait(wait))
}

func notFoundStub() *Stub {
	return NewStub().WithResponse(NewResponse().WithStatusCode(http.StatusNotFound))
}
