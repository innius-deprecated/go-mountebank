package mountebank

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	// GET HTTP method
	GET = "GET"

	// POST HTTP method
	POST = "POST"

	// PUT HTTP method
	PUT = "PUT"

	// DELETE HTTP method
	DELETE = "DELETE"

	// PATCH HTTP method
	PATCH = "PATCH"

	// HEAD HTTP method
	HEAD = "HEAD"

	// OPTIONS HTTP method
	OPTIONS = "OPTIONS"
)

// A condition that determines whether a given stub is responsible for responding. Each stub can have 0 or more predicates.
type Predicate struct {
	Equals *EqualPredicate `json:"equals,omitempty"`
}

// internal interface
type iPredicate interface {
	build() *Predicate
}

// NewPredicate returns a new predicate instance
func NewPredicate() *Predicate {
	return &Predicate{}
}

// EqualPredicate returns a new Equals predicate; this predicate specifies which request fields should match the predicate
type EqualPredicate struct {
	predicate *Predicate
	value     *equalPredicateValue
}

type equalPredicateValue struct {
	Method  string            `json:"method,omitempty"`
	Path    string            `json:"path,omitempty"`
	Headers map[string]string `json:"headers,omitempty"`
}

// Custom Marshal handler
func (p *EqualPredicate) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		*equalPredicateValue
	}{
		p.value,
	})
}

// WithEquals returns a new EqualPredicate
func (p *Predicate) WithEquals() *EqualPredicate {
	ep := &EqualPredicate{
		predicate: p,
		value:     &equalPredicateValue{Headers: map[string]string{}},
	}
	p.Equals = ep
	return ep
}

func (p *Predicate) build() *Predicate {
	return p
}

func (ep *EqualPredicate) WithMethod(method string) *EqualPredicate {
	ep.value.Method = method
	return ep
}

func (ep *EqualPredicate) WithHeader(header, value string) *EqualPredicate {
	ep.value.Headers[header] = value
	return ep
}

func (ep *EqualPredicate) WithPath(path string) *EqualPredicate {
	ep.value.Path = path
	return ep
}

func (ep *EqualPredicate) WithBearerToken(token string) *EqualPredicate {
	ep.value.Headers[http.CanonicalHeaderKey("authorization")] = fmt.Sprintf("bearer %v", token)
	return ep
}

func (ep *EqualPredicate) build() *Predicate {
	return ep.predicate
}
