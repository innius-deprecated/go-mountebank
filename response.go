package mountebank

import (
	"encoding/json"
	"net/http"
)

// NewResponse returns a new response instance
func NewResponse() *Response {
	return &Response{
		response:  &response{Headers: map[string]string{}},
		behaviors: &behaviors{},
	}
}

// Response represents a stub response; it can have a status code, headers and a body
type Response struct {
	response  *response
	behaviors *behaviors
}

type behaviors struct {
	Wait int `json:"wait,omitempty"`
}

type response struct {
	StatusCode int               `json:"statusCode,omitempty"`
	Body       interface{}       `json:"body,omitempty"`
	Headers    map[string]string `json:"headers,omitempty"`
}

// Custom Marshal handler
func (r *Response) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		Behaviors *behaviors `json:"_behaviors,omitempty"`
		Is        *response  `json:"is,omitempty"`
	}{
		r.behaviors,
		r.response,
	})
}

// fluent api to define the status code of the response
func (r *Response) WithStatusCode(status int) *Response {
	r.response.StatusCode = status
	return r
}

// fluent api to define the body of the response
func (r *Response) WithBody(v interface{}) *Response {
	r.WithHeader(http.CanonicalHeaderKey("content-type"), "application/json") // set the header to json for convenience
	r.response.Body = v
	return r
}

// fluent api to define an header of the response
func (r *Response) WithHeader(name, value string) *Response {
	r.response.Headers[name] = value
	return r
}

// WithWait adds latency to a response by waiting a specified number of milliseconds before sending the response.
func (r *Response) WithWait(t int) *Response {
	r.behaviors = &behaviors{Wait: t}
	return r
}
