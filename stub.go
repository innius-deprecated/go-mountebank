package mountebank

// A stub represents a mountebank stub
type Stub struct {
	Predicates []*Predicate `json:"predicates,omitempty"`
	Responses  []*Response  `json:"responses,omitempty"`
}

func NewStub() *Stub {
	return &Stub{

		Predicates: []*Predicate{},
		Responses:  []*Response{},
	}
}

func (s *Stub) WithResponse(r *Response) *Stub {
	s.Responses = append(s.Responses, r)
	return s
}

func (s *Stub) WithPredicate(p iPredicate) *Stub {
	s.Predicates = append(s.Predicates, p.build())
	return s
}
