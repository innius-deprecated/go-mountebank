package test

import (
	"fmt"
	"net/http"
	"os"
	"testing"

	mb "bitbucket.org/to-increase/go-mountebank"
	"github.com/stretchr/testify/assert"
)

type Employee struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func TestMain(m *testing.M) {
	if !mb.IsRunning() {
		panic(fmt.Errorf("Mountebank is not running; see http://mbtest.org for installation instructions"))
	}
	os.Exit(m.Run())
}

func TestCreateNewImposter(t *testing.T) {
	emp := &Employee{ID: "1974", Name: "Ron van der Wijngaard"}

	stb := mb.NewStub().
		WithPredicate(
		mb.NewPredicate().
			WithEquals().WithPath("/test1234").
			WithHeader("Content-Type", "application/json").
			WithHeader("Authorization", "bearer xxx")).
		WithResponse(
		mb.NewResponse().
			WithStatusCode(200).
			WithWait(2000).
			WithBody(emp).
			WithHeader("Content-Type", "application/text"))

	imp := mb.NewImposter().WithStub(stb)
	n, err := imp.Create()

	assert.Nil(t, err)
	assert.NotZero(t, n.Port)
}

func TestCreateNewDefaultImposter(t *testing.T) {
	result := struct {
		Name string `json:"name"`
	}{"John Doe"}

	imp, err := mb.DefaultPostImposter("/item", result, http.StatusCreated)
	assert.Nil(t, err)
	assert.NotNil(t, imp)
}
